# Very short description of the package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/srice/api-wrapper.svg?style=flat-square)](https://packagist.org/packages/srice/api-wrapper)
[![Build Status](https://gitlab.com/srice/api-wrapper/badges/master/pipeline.svg)](https://gitlab.com/srice/api-wrapper/-/commits/master)
[![Quality Score](https://gitlab.com/srice/api-wrapper/badges/master/coverage.svg)](https://gitlab.com/srice/api-wrapper/-/commits/master)
[![Total Downloads](https://img.shields.io/packagist/dt/srice/api-wrapper.svg?style=flat-square)](https://packagist.org/packages/srice/api-wrapper)

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.

## Installation

You can install the package via composer:

```bash
composer require srice/api
```

## Usage

``` php
// Usage description here
```

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email contact@srice.eu instead of using the issue tracker.

## Credits

- [SRICE](https://github.com/srice)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
