<?php

namespace Srice\Api\Tests;

use Orchestra\Testbench\TestCase;
use Srice\Api\Api;
use Srice\Api\ApiServiceProvider;

class ExampleTest extends TestCase
{

    protected function getPackageProviders($app)
    {
        return [ApiServiceProvider::class];
    }

}
