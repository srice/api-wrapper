<?php

namespace Srice\Api;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Srice\Api\Skeleton\SkeletonClass
 */
class ApiFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'api';
    }
}
