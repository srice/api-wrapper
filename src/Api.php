<?php

namespace Srice\Api;

use Illuminate\Support\Facades\Http;

class Api
{
    /**
     * @var \Illuminate\Config\Repository
     */
    private $endpoint;
    /**
     * @var \Illuminate\Config\Repository
     */
    private $client_user;
    /**
     * @var \Illuminate\Config\Repository
     */
    private $client_pass;

    /**
     * @var $token
     */
    private $token;

    public function __construct()
    {
        $this->endpoint = config('api.endpoint');
        $this->client_user = config('api.client_email');
        $this->client_pass = config('api.client_pass');

        $this->getAuthentication();
    }

    private function getAuthentication()
    {
        $auth = Http::post($this->endpoint . 'login', [
            "email" => $this->client_user,
            "password" => $this->client_pass
        ])->json();

        $this->token = $auth['api_token'];
    }

    public function get($uri, $params = [])
    {
        return Http::get($this->endpoint . $uri, [
            "headers" => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $this->token
            ],
            "json" => $params
        ])->json();
    }

    public function post($uri, $params = [])
    {
        return Http::post($this->endpoint . $uri, [
            "headers" => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $this->token
            ],
            "json" => $params
        ])->json();
    }

    public function delete($uri, $params = [])
    {
        return Http::delete($this->endpoint . $uri, [
            "headers" => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $this->token
            ],
            "json" => $params
        ])->json();
    }
}
