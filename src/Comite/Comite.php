<?php


namespace Srice\Api\Comite;


use Srice\Api\Api;

class Comite extends Api
{
    public function create($name, $adresse, $cp, $ville, $tel, $email)
    {
        return $this->post('comite/create', [
            'name' => $name,
            'adresse' => $adresse,
            'codePostal' => $cp,
            'ville' => $ville,
            'tel' => $tel,
            'email' => $email,
        ]);
    }

    public function comite($id)
    {
        return $this->get('comite/' . $id);
    }

    public function update($id, $name = null, $adresse = null, $cp = null, $ville = null, $tel = null, $email = null)
    {
        return $this->post('comite/' . $id, [
            'name' => $name,
            'adresse' => $adresse,
            'codePostal' => $cp,
            'ville' => $ville,
            'tel' => $tel,
            'email' => $email,
        ]);
    }

    public function destroy($id)
    {
        return $this->delete('comite/' . $id);
    }
}
